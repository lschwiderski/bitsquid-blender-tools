# Bitsquid Blender Tools

**Check out the [Wiki](https://gitlab.com/lschwiderski/bitsquid-blender-tools/-/wikis/home)!**

## Install

Copy the `addons/bitsquid` directory to `$BLENDER/scripts/addons/bitsquid`, where `$BLENDER` is one of [Blender's configuration directories](https://docs.blender.org/manual/en/latest/advanced/blender_directory_layout.html#blender-directory-layout).
It should now show up in Blender's preferences as `Import-Export: Bitsquid Engine`.

### Development

For active development, you'll want to symlink the above, rather than copy. The scripts itself are already set up for proper reloading via the `System > Reload scripts` command in Blender.

## Goal

Extend Blender to become capable of creating various assets for the Bitsquid (and Stingray) game engine.

## Current Status

Very simple exports for `.unit` and `.material` files.
