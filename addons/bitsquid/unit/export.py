#    Bitsquid Blender Tools
#    Copyright (C) 2021  Lucas Schwiderski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import bpy
from bitsquid import step
from bpy_extras import io_utils
from bitsquid.material import export as material_export


template = """
%if len(materials) > 0:
materials = {
    %for item in materials:
        {{ item['name'] }} = "{{ item['path'] }}"
    %endfor
}
%endif

renderables = {
    {{ object.name }} = {
        always_keep = false
        culling = "bounding_volume"
        generate_uv_unwrap = false
        occluder = false
        shadow_caster = true
        surface_queries = false
        viewport_visible = true
    }
}
"""


def build_filepath(context, object, mode='ABSOLUTE'):
    project_root = bpy.path.abspath(context.scene.bitsquid.project_root)
    base_src = os.path.dirname(context.blend_data.filepath)
    print(base_src, bpy.path.abspath(project_root))
    filename = "{}.unit".format(object.name)
    return io_utils.path_reference(
        os.path.join(object.bitsquid.filepath, filename),
        base_src=base_src,
        base_dst=project_root,
        mode=mode,
    )


def save(self, context, object):
    filepath = build_filepath(context, object)
    self.report({'INFO'}, "Saving unit to " + filepath)

    materials = []
    for material_slot in object.material_slots.values():
        material = material_slot.material
        path = material_export.build_filepath(context, material, 'RELATIVE')
        materials.append({
            'name': material.name,
            'path': path,
        })

    namespace = {
        'object': object,
        'materials': materials,
    }
    content = step.Template(template, strip=False).expand(namespace)

    with open(filepath, "w", encoding="utf8", newline="\n") as f:
        f.write(content)

    return {'FINISHED'}
